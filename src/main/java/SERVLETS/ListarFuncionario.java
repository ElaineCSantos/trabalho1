/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SERVLETS;

import DAO.FuncionarioDAO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ElaineSantos
 */
public class ListarFuncionario implements Acao {
    public String executar(HttpServletRequest request, HttpServletResponse response){
        FuncionarioDAO funcDAO = new FuncionarioDAO();
        
        request.setAttribute("funcionarios", funcDAO.getFuncionarios());

        return "/ListaFuncionario.jsp";
    }

}
